<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'hague_cms');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'rebmasmash');

/** MySQL hostname */
define('DB_HOST', '127.0.0.1');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '37ok-L]b5G!0t%zq]?}#[P@w(ou b],D(Gb+IvCtS?-e@cr>N;b7uOqme@}K$>0W');
define('SECURE_AUTH_KEY',  'VFl/l:fpOKVJIjv}]9pNd{l9#%yMyxSus{@cvnk>!v#.gGyDK@6/A:N9nOdg&$Lu');
define('LOGGED_IN_KEY',    ']b^kOo~{@&&tayU<.x a;j6Tg{JSOl[zi0,xaVf+?$_?$!kMV<_Rk`7eRpV5xc,b');
define('NONCE_KEY',        '`w6j%2%JP9!hnz>uv&H?c+gQ w4m[URjN;q!9LdyGk?p)qY#)1cu.1qcswTWfE<l');
define('AUTH_SALT',        'Rw_$Y;#ut ~1>?~!N(EK^wSj>K|vz.)C>G-.)Q9yV?2{@(F9X&; OC-8kVC@ Ftp');
define('SECURE_AUTH_SALT', '%Dy0%rTEkB ()q&lhhZi8P3bj6O3+2>9xz~:0)._c3?_m6J7F|XfSW#_RVc6/p;L');
define('LOGGED_IN_SALT',   '^pm1C&]rjMBf)HT}pIg]j`eNH%f:#pd-0]Hqj_/aq>RMv]gv8|QBfq]OB-x-O}!{');
define('NONCE_SALT',       'mK~qnqY5=P;Eua=[kE1^E0r}X$P#!?4cbaGj%D|yL>)wBYBV?m1Fo2$0h:rk-8f4');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
