<footer>
	<nav>
        <?php
            wp_nav_menu(
                array(
                    'container' => false,
                    'menu' => __('Footer Menu'),
                    'theme_location' => 'footer-nav',
                )
            );
        ?>                  
    </nav>
</footer>
<?php wp_footer(); ?>
</body>
</html>