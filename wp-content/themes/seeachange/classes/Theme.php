<?php

class Theme {

    public static function init() {
        // Assets
        add_action('wp_print_styles', 'Theme::loadAssets');
        // Menus
        add_action('after_setup_theme', 'Theme::registerMenus');
        // Sidebars
        // add_action('widgets_init', 'Theme::registerSidebars');

        Theme::customPostTypes();

        return;

    }

    public static function loadAssets() {
        global $wp_styles;
        if (!is_admin()) {
            wp_enqueue_script('jquery');

            wp_register_style('flickity-css', "https://unpkg.com/flickity@2/dist/flickity.min.css");
            wp_register_style('main-styles', get_template_directory_uri() . '/assets/css/styles.css', array(), '1.0.0');


            wp_register_script('flickity-js', "https://unpkg.com/flickity@2/dist/flickity.pkgd.min.js");
            wp_register_script('main-js', get_stylesheet_directory_uri() . '/assets/js/scripts.min.js', array('jquery'), '1.0.0',true);
         
            wp_enqueue_style('flickity-css');
            wp_enqueue_style('main-styles');

            wp_enqueue_script('flickity-js');
            wp_enqueue_script('main-js');

        }
        return;
    }

    public static function registerMenus() {
        // wp menus
        add_theme_support('menus');
        register_nav_menus(
                array(
                    'main-nav' => __('The Main Menu'), // main nav in header                    
                    'footer-nav' => __('Footer Menu'), // secondary nav in footer
                )
        );
        return;
    }

    public static function customPostTypes() {

        add_theme_support( 'post-thumbnails' );
        
        function custom_products() {
            $supports = array('title', 'thumbnail');
            register_post_type( 'products',
                array(
                    'labels' => array(
                        'name' => __( 'Products' ),
                        'singular_name' => __( 'Product' )
                    ),
                    'public' => true,
                    'description' => __('This is the products post type', 'seeachange'),
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'products'),
                    'menu_icon' => 'dashicons-admin-site',
                    'supports' => $supports,
                    'taxonomies' => array( 'category', 'post_tag' ),

                )
            );
        }

        function custom_partners() {
            $supports = array('title');
            register_post_type( 'partners',
                array(
                    'labels' => array(
                        'name' => __( 'Partners' ),
                        'singular_name' => __( 'Partner' )
                    ),
                    'public' => true,
                    'description' => __('This is the partners post type', 'seeachange'),
                    'has_archive' => true,
                    'rewrite' => array('slug' => 'partners'),
                    'menu_icon' => 'dashicons-groups',
                    'supports' => $supports,
                )
            );
        }

        add_action( 'init', 'custom_products' );
        add_action( 'init', 'custom_partners' );

        return;
    }
}

?>