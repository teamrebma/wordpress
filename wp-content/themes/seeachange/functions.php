<?php

	foreach (glob(get_template_directory() . '/classes/*.php') as $filename) {
	    require_once $filename;
	}

	acfFunctions::init();
	Theme::init();
	Debug::init();

?>