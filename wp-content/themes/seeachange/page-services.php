<?php
// Template Name: Services Page
get_header();

?>
<main>


	<?php 

		$firstSection = get_field('services_page')[0];

		if($firstSection) {


			echo '<div id="first-section">
				<div class="container">
				 	' . ($firstSection['heading'] ? '<h1>' . $firstSection['heading'] . '<h1>' : '') .'
				 	' . $firstSection['content'] . '
				 	<img src="' . $firstSection['image'] . '" />
				 	'. ($firstSection['button'][0]['url'] && $firstSection['button'][0]['text'] ? '[<a class="button" href="'. $firstSection['button'][0]['url'] .'">'.  $firstSection['button'][0]['text'] .'</a>' : '' ) . '
				</div>
			</div>';


		}


	?>


</main>

<?php
	get_footer();
?>