<?php
get_header();

$category = get_the_category()[0];

$partner = get_field('partner')[0];
$product = get_field('product')[0];
$country = reset($product['country']);
$description = $product['description'];
$detailTypes = $product['detail_types'];
$productDetails = $product['product_details'][0];

Debug::dump($category);

?>
<main>
	<?php echo '
		
		<div class="banner" style="background-image: url(' . get_the_post_thumbnail_url() . ');">
			<a href="#">' . $country . '</a>
			| <a href="' . get_permalink($partner->ID) . '">' . $partner->post_title . '</a>
			<h1>' . get_the_title() . '</h1>
		</div>
	
	'; ?>

	<?php echo '
		<div id="product-wrapper">
			<div class="container">
				<div id="product-information">
					<div>
						<img src="' . get_template_directory_uri().'/assets/icons/product-type.png' . '"
						<h2>TYPE</h2>
						<a href="">' . $category->name . '</a>
					</div>
				</div>
				<div id="product-left">
					<div id="product-description">' . $description . '</div>
					<div id="product-details">';

						if($productDetails) {
							foreach($productDetails as $productDetail) {

								$heading = '';
								$icon = '';
								$content = '';

								if($productDetail) {
									if(in_array('Whats Included', $detailTypes) && $productDetail === $productDetails['whats_included']) {
										$heading = 'What’s Included';
										$icon = get_template_directory_uri().'/assets/icons/whats-included.png';
									}

									if(in_array('Program Duration', $detailTypes) && $productDetail === $productDetails['program_duration']) {
										$heading = 'Program Duration';
										$icon = get_template_directory_uri().'/assets/icons/program-duration.png';
									}

									if(in_array('Trip Itinerary', $detailTypes) && $productDetail === $productDetails['trip_itinerary']) {
										$heading = 'Trip Itinerary';
										$icon = get_template_directory_uri().'/assets/icons/trip-itinerary.png';
									}

									if(in_array('Awards / Testimonials', $detailTypes) && $productDetail === $productDetails['awards_testimonials']) {
										$heading = 'Awards/Testimonials';
										$icon = get_template_directory_uri().'/assets/icons/awardstestimonials.png';
									}
								
									if($heading) {
										echo '<div class="detail-type">
											<div class="detail-toggle"><img src="' . $icon . '" /><h3>' . $heading . '</h3></div>
											<div class="detail-content">'. $productDetail .'</div>
										</div>';
									}

								}
							}

							if(in_array('Inquire', $detailTypes)) {
								$heading = 'Inquire';
								$icon = get_template_directory_uri().'/assets/icons/inquire.png';

								echo '<div class="detail-type">
									<div class="detail-toggle"><img src="' . $icon . '" />' . $heading . '</div>
									<div class="detail-content"></div>
								</div>';
							}

						}

					echo '</div>
				</div>
				<div id="product-right">
					
				</div>
			</div>
		</div>';
	?>
</main>

<?php
	get_footer();
?>
