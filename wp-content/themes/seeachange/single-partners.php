<?php
get_header();

	$post = get_post();
	$partnerInfo = get_field('partner_information')[0];
	$country = get_field('country');
	$websiteUrl = get_field('website_url');
	$bannerImage = get_field('banner_image');

	$args = array(
		'post_type'		=> 'products',
		'posts_per_page'	=> -1,
		'meta_query'		=> array(
		array(
			'key' => 'partner',
			'value' => '"' . get_the_ID() . '"',
			'compare' => 'LIKE'
			)
		)
	);

	$wp_query = new WP_Query($args);
	$products = $wp_query->get_posts();

?>
<main>

	<style type="text/css">
		
		.secondary-banner {height:500px;}

	</style>

	<?php

	if(!$bannerImage) {
		$bannerImage = get_field('partner_backup_banner', 'options');
	}

	echo '
		<div class="secondary-banner section-image" '. ($bannerImage ? 'style="background-image: url('. $bannerImage .');"' : '') .'>
			<div class="container">
				<h1></h1>
			</div>
		</div>
	';

	if($partnerInfo) {
		echo '
			<div id="partner-information">
				<div class="container">
					' . ($partnerInfo['logo'] ? '<img src="' . $partnerInfo['logo'] . '" />' : '') . '
					' . ($partnerInfo['content'] ? $partnerInfo['content'] : '') . '
				</div>
			</div>
		';
	}


	if($products) {
		echo '<div id="partner-products>
			<div class="container">
				<h3>O';
				
				foreach($products as $product) {
					$p = get_field('product', $product->ID)[0];

					echo '
						' . $p['description'] . '
					';
				}

		echo '</div>
		</div>';
	}
   


?>

</main>

<?php
get_footer();
?>
